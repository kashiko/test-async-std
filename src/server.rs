use async_std::{
    net::{TcpListener, TcpStream},
    sync::Arc,
    task,
};
use futures::{
    channel::mpsc::{self, Receiver},
    io::{BufReader, BufWriter},
    prelude::*,
    sink::SinkExt,
};

const BUF_LEN: usize = 0x80000;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let addr = if args.len() < 2 {
        "127.0.0.1:8080".to_string()
    } else {
        args[1].clone()
    };
    println!("Start server");
    task::block_on(spawn_and_log_error(accept_loop(addr)));
}

pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Result<T> = std::result::Result<T, Error>;

async fn accept_loop(addr: String) -> Result<()> {
    let listener = TcpListener::bind(addr).await?;
    let mut incoming = listener.incoming();

    println!("Server started");
    while let Some(stream) = incoming.next().await {
        let stream = stream?;
        let stream = Arc::new(stream);
        spawn_and_log_error(connection_loop(stream));
    }
    println!("Server stopped");

    Ok(())
}

async fn connection_loop(stream: Arc<TcpStream>) -> Result<()> {
    println!("Connected");

    let (mut sender, receiver) = mpsc::channel::<u64>(0x10000);
    let handle = spawn_and_log_error(writer_loop(Arc::clone(&stream), receiver));

    let reader = &mut BufReader::with_capacity(BUF_LEN, &*stream);

    println!("Start reading");
    loop {
        let mut bytes = [0; 8];
        reader.read_exact(&mut bytes).await?;
        let len = u64::from_be_bytes(bytes);

        let mut payload = Vec::new();
        let mut r = reader.take(len);
        r.read_to_end(&mut payload).await?;

        println!("  read: {:x}", len);

        sender.send(len).await?;

        if len == 0 {
            // end mark
            break;
        }
    }
    println!("End reading");

    handle.await;

    println!("Disconnected");
    Ok(())
}

async fn writer_loop(stream: Arc<TcpStream>, mut receiver: Receiver<u64>) -> Result<()> {
    let writer = &mut BufWriter::with_capacity(BUF_LEN, &*stream);

    println!("Start writing");
    while let Some(len) = receiver.next().await {
        writer.write_all(&len.to_be_bytes()).await?;
        let buf = vec![0; len as usize];
        writer.write_all(&buf).await?;
        writer.flush().await?;

        println!("  wrote: {:x}", len);

        if len == 0 {
            // end mark
            break;
        }
    }
    println!("End writing");

    Ok(())
}

fn spawn_and_log_error<F>(fut: F) -> task::JoinHandle<()>
where
    F: Future<Output = Result<()>> + Send + 'static,
{
    task::spawn(async move {
        if let Err(e) = fut.await {
            eprintln!("{}", e)
        }
    })
}
