use async_std::{net::TcpStream, sync::Arc, task};
use futures::{
    io::{BufReader, BufWriter},
    prelude::*,
};

const BUF_LEN: usize = 0x80000;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let n: usize = if args.len() < 2 {
        64
    } else {
        args[1].parse().unwrap()
    };
    let addr = if args.len() < 3 {
        "127.0.0.1:8080".to_string()
    } else {
        args[2].clone()
    };
    println!("Start client");
    task::block_on(spawn_and_log_error(connection_loop(addr, n)));
}

pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Result<T> = std::result::Result<T, Error>;

async fn connection_loop(addr: String, n: usize) -> Result<()> {
    let stream = TcpStream::connect(addr).await?;
    let stream = Arc::new(stream);

    let handle = spawn_and_log_error(reader_loop(Arc::clone(&stream)));

    let mut writer = BufWriter::with_capacity(BUF_LEN, &*stream);

    println!("Start writing");
    for i in 0..n {
        let len: u64 = 0x20000 + 0x01000 * (i as u64);
        writer.write_all(&len.to_be_bytes()).await?;
        let buf = vec![0; len as usize];
        writer.write_all(&buf).await?;
        writer.flush().await?;

        println!("  wrote: {:x}", len);
    }
    // end mark
    writer.write_all(&0_u64.to_be_bytes()).await?;
    writer.flush().await?;
    println!("End writing");

    handle.await;

    Ok(())
}

async fn reader_loop(stream: Arc<TcpStream>) -> Result<()> {
    let reader = &mut BufReader::with_capacity(BUF_LEN, &*stream);

    println!("Start reading");
    loop {
        let mut bytes = [0; 8];
        reader.read_exact(&mut bytes).await?;
        let len = u64::from_be_bytes(bytes);
        if len == 0 {
            // end mark
            break;
        }

        let mut payload = Vec::new();
        let mut r = reader.take(len);
        r.read_to_end(&mut payload).await?;

        println!("  read: {:x}", len);
    }
    println!("End reading");

    Ok(())
}

fn spawn_and_log_error<F>(fut: F) -> task::JoinHandle<()>
where
    F: Future<Output = Result<()>> + Send + 'static,
{
    task::spawn(async move {
        if let Err(e) = fut.await {
            eprintln!("{}", e)
        }
    })
}
